# CI CD using gitlab example

Example of a basic usage of the CI/CD using gitlab-ci.yml

## Steps

1. Create a `Dockerfile` for your application
2. Test your `Dockerfile`, e.g. `docker build . -t deepcube-example`
3. Test the container image, e.g. `docker run --rm -p 1323:1323 deepcube-example`
4. Automate these steps using `.gitlab-ci.yml`
5. The created container image will be available directlty in the gitlab/container images page on the project


